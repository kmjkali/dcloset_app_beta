import 'package:dclost_app/model/mypage/mypage_item.dart';
import 'package:flutter/material.dart';
class ComponentMyPageItem extends StatelessWidget {
  const ComponentMyPageItem({
    super.key,
    required this.myPageItem,
    required this.callback
  });

  final MyPageItem myPageItem;
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Center(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Container
                  (decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                  )
                ),
                  padding: EdgeInsets.all(20),
                    child: Text(myPageItem.myPageTitle),),
              ],
            ),
          ),

        ),
      ),
    );
  }
}
