
import 'package:flutter/material.dart';

import '../model/goods/goods_item.dart';

class ComponentGoodsCard extends StatelessWidget {
  const ComponentGoodsCard({
    super.key,
    required this.callback, required this.goodsItem
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            SizedBox(
              height:130,
              child: Image.network(
                  goodsItem.productMainImage,
                  fit:BoxFit.fitHeight


              ),
            ),

            /*Container(
              child: Text(
                  goodsItem.productInfo,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              softWrap: false,
              style: TextStyle(
                fontSize: 10
              ),),
            ),*/
          ],
        ),
      ),
    );
  }
}
