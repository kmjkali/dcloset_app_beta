import 'package:dclost_app/model/goods/goods_item.dart';
import 'package:flutter/material.dart';

// 제품 리스트 보여주기 폼

class ComponentGoodsCard extends StatelessWidget {
  const ComponentGoodsCard({
    super.key,
    required this.callback, required this.goodsItem
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
              width: 110,
              height: 110,

              child: Image.network(
                goodsItem.productMainImage,
                fit:BoxFit.fitHeight
              ),
            ),
            Container(
              child: Text(goodsItem.productName,style: const TextStyle(fontSize: 10),),
            ),
            /*Container(
              child: Text(
                  goodsItem.productInfo,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              softWrap: false,
              style: TextStyle(
                fontSize: 10
              ),),
            ),*/
          ],
        ),
      ),
    );
  }
}
