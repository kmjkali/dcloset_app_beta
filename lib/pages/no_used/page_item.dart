import 'package:flutter/material.dart';

/**
 * 메인 페이지 연습삼아 만들어 놓은 거에용 ㅎ
 */

class PageItem extends StatefulWidget {
  const PageItem({super.key});

  @override
  State<PageItem> createState() => _PageItemState();
}

class _PageItemState extends State<PageItem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset('assets/logo.png'),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        backgroundColor: Colors.white,
      ),
      body: Container(
        child:
            Container(
              padding:EdgeInsets.all(7),
              margin: EdgeInsets.all(7),
                child: Column(
                  children: [
                    Text(
                        style: TextStyle(fontSize: 15,fontStyle: FontStyle.italic),'"당신만의 놀라운 Dcloset"',textAlign: TextAlign.center,),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Image.asset('assets/main2.png',width: 450, height: 430, fit: BoxFit.fill,),
                            Text(
                                style: TextStyle(fontWeight: FontWeight.bold),'Dcloset Cataloge')
                          ],
                        ),
                      ),
                    Container(

                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/codi1.png',width: 100, height: 100,),
                          Image.asset('assets/codi2.png',width: 100, height: 100,),
                          Image.asset('assets/codi3.png',width: 100, height: 100,),
                          Image.asset('assets/codi4.png',width: 100, height: 100,),
                        ], // 코디 이미지 4개
                      ),
                    ),

                  ],
                ),
            )
        ),
      );
  }
}
