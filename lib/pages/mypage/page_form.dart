import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/pages/member/page_member_type.dart';
import 'package:dclost_app/pages/mypage/page_member_correct.dart';
import 'package:dclost_app/pages/question/page_question.dart';
import 'package:flutter/material.dart';

// 마이페이지 폼

class PageForm extends StatefulWidget {
  const PageForm({super.key});

  @override
  State<PageForm> createState() => _PageFormState();
}

class _PageFormState extends State<PageForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(30),
                child: Text(
                    style: TextStyle(fontSize: 24  ),'마이 페이지'),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      //엘리베이터 버튼 스타일
                      minimumSize: Size(200, 50), // 버튼 사이즈 조절
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMemberCorrect()));
                    },
                    child: Text('회원 정보 수정')),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      //엘리베이터 버튼 스타일
                      minimumSize: Size(200, 50), // 버튼 사이즈 조절
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    onPressed: () {Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageQuestion()));},
                    child: Text('내 문의 내역')),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      //엘리베이터 버튼 스타일
                      minimumSize: Size(200, 50), // 버튼 사이즈 조절
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => MemberType()));
                    },
                    child: Text('나의 회원 정보')),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
