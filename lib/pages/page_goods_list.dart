import 'package:dclost_app/components/component_goods_card.dart';
import 'package:dclost_app/pages/page_goods_detail.dart';
import 'package:dclost_app/repository/repo_goods.dart';
import 'package:flutter/material.dart';

import '../model/goods/goods_item.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();

}


class _PageGoodsListState extends State<PageGoodsList> {

  List<GoodsItem> _list = [];

  /*Future<void> _loadList() async {
    await RepoGoods().getGoodsList()
        .then((res) => {
          setState(() {_list = res.list;})})
=======
        .catchError(err) => {debugPrint(err)})
  }*/

  Future<void> _loadList() async {

    await RepoGoods().getGoodsList()
        .then((res) => {
            setState(() {
                print(res);
                if ( res.code == 0 ){
                  _list = res.list;
                } else {
                  //Fluttertoast.showToast(msg: res.msg);
                }
            })
        });
  }

  @override
  void initState(){
    super.initState();

    // 상품목록조회
    print("request good slist ");
    _loadList();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('코디 리스트'),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 3, // 1개 행에 항목 3개씩
        mainAxisSpacing: 1,
        crossAxisSpacing: 4,
        childAspectRatio: 0.7,

        children: _list.map((item) {

          // @TODO logoicon 대신 파일 없을 때 조회할 이미지 url 넣기
          String productMainImage = item.productMainImage ?? "logoicon.png";

          return ComponentGoodsCard(callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(id:item.id)));
            //@TODO 클릭했을 때 실행하는거 구현
          }, goodsItem: item,);

        }).toList(),
        /*children: List.generate(
          4, // 항목 개수
              (int index) {
            return Card(
              elevation: 2,
              child: Column(
                children: <Widget>[
                  Expanded(child: Image.asset('assets/catalog1.png',width: 200, height: 200,),flex: 5,
                  ),
                  Expanded(child: ListTile(
                   title: Text('꾸안꾸 코디세트'),
                    subtitle: Text('25,000원'),
                    ),
                  ),
                ],
              ),
            );
          },
        ),*/
      ),
    );
  }
}

