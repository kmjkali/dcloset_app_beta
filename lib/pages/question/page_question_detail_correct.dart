import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/model/question/question_form.dart';
import 'package:flutter/material.dart';

class PageQuestionDetailCorrect extends StatefulWidget {
  const PageQuestionDetailCorrect({
    super.key,
    required this.questionForm
  });

  final QuestionForm questionForm;

  @override
  State<PageQuestionDetailCorrect> createState() => _PageQuestionDetailCorrectState();
}

/// Q & A 수정 페이지 입니다.


class _PageQuestionDetailCorrectState extends State<PageQuestionDetailCorrect> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
           Container(
               width: 500,
               decoration: BoxDecoration(
                     border: Border.all(
                       width: 2,
                       color: Colors.black12,
                ),
                    ),
                     child: Column(
                         children: [
                        Container(
                       padding: EdgeInsets.all(10),
                         width: 500,
                          decoration: BoxDecoration(
                         border: Border.all(
                             width: 1,
                         color: Colors.black12,
                           ),
                          ),
                            child: Text(
                        widget.questionForm.questionTitle,
                        style: TextStyle(
                        fontSize: 18,
                          ),
                         ),
                        ),
                       Container(
                          padding: EdgeInsets.all(5),
                        width: 500,
                         decoration: BoxDecoration(
                         border: Border.all(
                          width: 1,
                        color: Colors.black12,
                          ),
                         ),
                          child: Text('${widget.questionForm.questionCreateDate.year}-${widget
                            .questionForm.questionCreateDate.month}-${widget.questionForm
                                                .questionCreateDate.day}'),
                         ),
                       Container(
                       padding: EdgeInsets.all(5),
                          width: 500,
                        decoration: BoxDecoration(
                           border: Border.all(
                           width: 1,
                        color: Colors.black12,
                          ),
                        ),
                           child: Text(widget.questionForm.questionMemberId),
                        ),
                         Container(
                            width: 500,
                            alignment: Alignment.center,
                          decoration: BoxDecoration(
                           border: Border.all(
                          width: 1,
                           color: Colors.black12,
                               ),
                          ),
                           child: Text('내용'),
                                            ),
                              Container(
                                    width: 500,
                                     height: 500,
                                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                   child: Text(widget.questionForm.questionContent),
                              ),
                             ],
                          ),
              ),
           ],
        ),
      )
    );
  }
}
