import 'package:dclost_app/model/goods/goods_response.dart';
import 'package:dclost_app/pages/page_order_complete.dart';
import 'package:dclost_app/repository/repo_goods.dart';
import 'package:flutter/material.dart';



class PageGoodsDetail extends StatefulWidget {

  const PageGoodsDetail({super.key, required this.id});

  final num id;

  // 제품 상세보기 페이지
  // 하단 주문하기 버튼 만들어야 함. ( 바텀네비바? 고정 버튼? 드로어?.. )
  // 제품 상세 정보 내용 좀더 그럴싸하게 디자인해야 함.

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {

  List<String> dropdownList = ['small', 'medium', 'large'];
  String selectedDropdown = 'small';

  List<String> dropdownColorList =['pink','white','black', 'grey'];
  String selectedDropColor ='pink';

  GoodsResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoGoods().getGoods(widget.id)
        .then((res) => {
      setState(() {
        if ( res.code == 0 ){
          _detail = res.data;
        } else {
          //Fluttertoast.showToast(msg: res.msg);
          // @TODO 예외처리
        }
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Image.network(_detail!.productMainImage),//
                  Text(
                     _detail!.productName
                  ),
                  Text(
                    _detail!.productInfo
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Image.network(_detail!.productSubImage1),
                  Image.network(_detail!.productSubImage2),
                  Image.network(_detail!.productSubImage3)
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('사이즈를 선택해주세요!'),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: DropdownButton(
                      value: selectedDropdown,
                      items: dropdownList.map((String item) {
                        return DropdownMenuItem<String>(
                          child: Text('$item'),
                          value: item,
                        );
                      }).toList(),
                      onChanged: (dynamic value) {
                        setState(() {
                          selectedDropdown = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('색상을 선택해주세요!'),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: DropdownButton(
                            value: selectedDropColor,
                            items: dropdownColorList.map((String item) {
                              return DropdownMenuItem<String>(
                                child: Text('$item'),
                                value: item,
                              );
                            }).toList(),
                            onChanged: (dynamic value) {
                              setState(() {
                                selectedDropColor = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        //엘리베이터 버튼 스타일
                        minimumSize: Size(100 ,75), // 버튼 사이즈 조절
                        backgroundColor: Colors.black,
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                      ),
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageOrderComplete()));
                      },child: Text('주문하기'),
                    ),
                  ),
                ],
              ),

            ),

          ],
        ),


      ),
    );
  }
}

