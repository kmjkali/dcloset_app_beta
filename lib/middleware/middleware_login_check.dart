import 'package:dclost_app/functions/token_lib.dart';
import 'package:dclost_app/pages/no_used/page_idex.dart';
import 'package:dclost_app/pages/page_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();
    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
          (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageIndex()),
          (route) => false);
    }
  }
}
