

class NoticeItem {

   num id;
   num memberId;
   String memberName;

   DateTime noticeCreateDate;
   String noticeTitle;
   String noticeContent;
   String noticeImage;

   NoticeItem(
       this.id,
       this.memberId,
       this.memberName,
       this.noticeCreateDate,
       this.noticeTitle,
       this.noticeContent,
       this.noticeImage);

   factory NoticeItem.fromJson(Map<String,dynamic>json){
     return NoticeItem(
         json['id'],
         json['memberId'],
         json['memberName'],
         json['noticeCreateDate'],
         json['noticeTitle'],
         json['noticeContent'],
         json['noticeImage']
     );
   }

}