class QuestionForm{

  // Q&A 게시판 단수 R (자세히 보기) 양식 폼.

  num id;
  DateTime questionCreateDate;
  String questionMemberId;
  String questionTitle;
  String questionContent;

  QuestionForm(this.id, this.questionCreateDate, this.questionMemberId, this.questionTitle, this.questionContent);

}
