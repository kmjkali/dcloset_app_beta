class QuestionMemberResponse{

   num memberId;
   DateTime questionCreateDate;
   String questionTitle;
   num questionPassword;
   String questionContent;
   String questionStatus;

   QuestionMemberResponse(
       this.memberId,
       this.questionCreateDate,
       this.questionTitle,
       this.questionPassword,
       this.questionContent,
       this.questionStatus
       );

   factory QuestionMemberResponse.fromJson(Map<String,dynamic>json){
     return QuestionMemberResponse(
         json['memberId'],
         json['questionCreateDate'],
         json['questionTitle'],
         json['questionPassword'],
         json['questionContent'],
         json['questionStatus']);
   }


}