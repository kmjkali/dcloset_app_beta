
class GoodsResponse{

  num id;
  String productName;
  String productInfo;
  String productColor;
  String goodsSize;
  String productMainImage;
  String productSubImage1;
  String productSubImage2;
  String productSubImage3;
  bool ynFree;
  bool ynMembership;
  bool ynOffline;
  bool ynOneDay;
  bool ynPost;

  GoodsResponse(this.id,this.productName,this.productInfo,this.productColor,
      this.goodsSize,this.productMainImage,
      this.productSubImage1,this.productSubImage2,this.productSubImage3,
      this.ynFree,this.ynMembership,this.ynOffline,this.ynOneDay,this.ynPost);

  factory GoodsResponse.fromJson(Map<String,dynamic>json) {
    return GoodsResponse(
        json['id'],
        json['productName'],
        json['productInfo'],
        json['productColor'],
        json['goodsSize'],
        json['productMainImage'],
        json['productSubImage1'],
        json['productSubImage2'],
        json['productSubImage3'],
        json['ynFree'],
        json['ynMembership'],
        json['ynOffline'],
        json['ynOneDay'],
        json['ynPost']
    );

  }

}